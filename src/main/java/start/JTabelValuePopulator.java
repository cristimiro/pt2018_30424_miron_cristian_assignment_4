package start;

import java.lang.reflect.Field;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * This is the reflection class
 * 
 * @author Cristian Miorn
 *
 */
public class JTabelValuePopulator {
	/**
	 * 
	 * @param tableHeaderClass
	 *            the class we want to create a table for
	 * @param tableRowsAsList
	 *            the arrayList with all the object we want in the table
	 * @return
	 */
	public <I> JTable createTable(final Class<I> tableHeaderClass, final List<I> tableRowsAsList) {
		final Object[] tableHeader = createTableHeader(tableHeaderClass);
		final List<Object[]> tableValues = createTableValues(tableHeaderClass, tableRowsAsList);
		final DefaultTableModel model = new DefaultTableModel(tableHeader, 0);
		for (final Object[] tableValueRow : tableValues) {
			model.addRow(tableValueRow);
		}
		return new JTable(model);
	}

	/**
	 * 
	 * @param tableHeaderClass
	 *            the class we want to create a header for
	 * @return an array containing header names
	 */
	private <I> Object[] createTableHeader(final Class<I> tableHeaderClass) {
		final Field[] declaredFields = tableHeaderClass.getDeclaredFields();
		final Set<String> headerNames = new LinkedHashSet<String>();
		for (final Field declaredField : declaredFields) {
			headerNames.add(declaredField.getName());
		}
		return headerNames.toArray();
	}

	/**
	 * 
	 * @param tableHeaderClass
	 *            the class we want to create a table for
	 * @param tableRowsAsList
	 *            the arrayList with all the object we want in the table
	 * @return returns the table values
	 */
	private <I> List<Object[]> createTableValues(final Class<I> tableHeaderClass, final List<I> tableRowsAsList) {
		final List<Object[]> tableValues = new LinkedList<Object[]>();
		for (final I objectAsRow : tableRowsAsList) {
			final List<String> objectFieldsAsRow = createObjectFieldsAsRow(tableHeaderClass, objectAsRow);
			tableValues.add(objectFieldsAsRow.toArray());
		}
		return tableValues;

	}

	/**
	 * 
	 * @param tableHeaderClass
	 *            the class we want to create a table for
	 * @param objectAsRow
	 *            object as row
	 * @return object fields as row list
	 */

	private <I> List<String> createObjectFieldsAsRow(final Class<I> tableHeaderClass, final I objectAsRow) {
		final List<String> objectFieldsAsRowList = new LinkedList<String>();
		final Field[] declaredFields = tableHeaderClass.getDeclaredFields();
		for (final Field declaredField : declaredFields) {
			final String fieldValueFromObject = getFieldValueFromObject(declaredField, objectAsRow);
			objectFieldsAsRowList.add(fieldValueFromObject);
		}
		return objectFieldsAsRowList;
	}

	/**
	 * 
	 * @param declaredField
	 *            field containing the desired value
	 * @param object
	 *            the object
	 * @return returns the value of a field
	 */
	private <I> String getFieldValueFromObject(final Field declaredField, final I object) {
		String value = "";
		if (declaredField != null) {
			try {
				declaredField.setAccessible(true);
				final Object objectFieldValue = declaredField.get(object);
				if (objectFieldValue != null) {
					final String objectFieldValueAsString = objectFieldValue.toString();
					if (objectFieldValueAsString != null) {
						value = objectFieldValueAsString;
					}
				}
				declaredField.setAccessible(false);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// log the error
				// value stays empty
			}
		}
		return value;

	}
}
