package mainPackage;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

/**
 * This is the class implementing a Client
 * 
 * @author Cristian Miorn
 *
 */
public class Client implements Serializable, Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private int clientId;
	private String clientName;
	private String clientMail;
	private String clientPhoneNumber;
	private int clientAge;

	/**
	 * The client details:
	 * 
	 * @param clientId
	 *            Each client has a unique id
	 * @param clientName
	 *            Each one has a name
	 * @param clientMail
	 *            Each one has a mail
	 * @param clientPhoneNumber
	 *            Each one has a phone number
	 * @param clientAge
	 *            Each one has a age
	 */

	public Client(int clientId, String clientName, String clientMail, String clientPhoneNumber, int clientAge) {
		this.clientId = clientId;
		this.clientName = clientName;
		this.clientMail = clientMail;
		this.clientPhoneNumber = clientPhoneNumber;
		this.clientAge = clientAge;
	}

	/**
	 * The default constructor
	 */
	public Client() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		return this.clientId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (clientId != other.clientId)
			return false;
		return true;
	}

	/**
	 * Returns the ID of the client
	 * 
	 * @return
	 */
	public int getClientId() {
		return clientId;
	}

	/**
	 * Sets a new Id for the client
	 * 
	 * @param clientId
	 */
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	/**
	 * Returns the Name of the client
	 * 
	 * @return
	 */
	public String getClientName() {
		return clientName;
	}

	/**
	 * Sets a new Name for the client
	 * 
	 * @param clientName
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * Returns the Mail of the client
	 * 
	 * @return
	 */
	public String getClientMail() {
		return clientMail;
	}

	/**
	 * Sets a new Mail for the client
	 * 
	 * @param clientMail
	 */
	public void setClientMail(String clientMail) {
		this.clientMail = clientMail;
	}

	/**
	 * Returns the Phone Number of the client
	 * 
	 * @return
	 */
	public String getClientPhoneNumber() {
		return clientPhoneNumber;
	}

	/**
	 * Sets a new Phone Number for the client
	 * 
	 * @param clientPhoneNumber
	 */
	public void setClientPhoneNumber(String clientPhoneNumber) {
		this.clientPhoneNumber = clientPhoneNumber;
	}

	/**
	 * Returns the Age of the client
	 * 
	 * @return
	 */
	public int getClientAge() {
		return clientAge;
	}

	/**
	 * Sets a new Age for the client
	 * 
	 * @param clientAge
	 */
	public void setClientAge(int clientAge) {
		this.clientAge = clientAge;
	}

	/**
	 * Overrides the "toString" Method
	 * 
	 * @Override
	 */
	public String toString() {
		String s = "";
		s = " ID - " + getClientId() + ", Name - " + getClientName() + ", Mail - " + getClientMail() + ", Phone - "
				+ getClientPhoneNumber() + ", age - " + getClientAge() + ".";
		return s;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof Account){
		}
		
	}

}
