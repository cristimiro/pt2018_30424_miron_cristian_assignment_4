package mainPackage;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import start.JTabelValuePopulator;

/**
 * This is the user interface class
 * @author Cristian Miorn
 *
 */
public class functionFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	// LABELS
	JLabel clientIdLabel = new JLabel("clientId");
	JLabel clientNameLabel = new JLabel("Name");
	JLabel clientMailLabel = new JLabel("Mail");
	JLabel clientPhoneLabel = new JLabel("Phone no.");
	JLabel clientAgeLabel = new JLabel("Age");

	JLabel accountIdLabel = new JLabel("Account Id");
	JLabel accountClientIdLabel = new JLabel("Client Id");
	JLabel accountTypeLabel = new JLabel("Type");
	JLabel accountAmountLabel = new JLabel("Amount");
	
	JLabel operationAccountClientIdLabel = new JLabel("Client Id");
	JLabel operationAccountIdLabel = new JLabel("Acc Id");
	JLabel operationAmountLabel = new JLabel("Amount");

	// BUTTONS

	JButton clientButton = new JButton("Clients"); // Creat all the buttons, textfields and labels you need
	JButton accountButton = new JButton("Accounts");
	JButton operationButton = new JButton("Operation");


	JButton clientAddButton = new JButton("Add Client");
	JButton clientRemoveButton = new JButton("Remove Client");
	JButton clientEditButton = new JButton("Edit Client");
	JButton clientBackButton = new JButton("Back to menu");

	JButton accountAddButton = new JButton("Add Account");
	JButton accountRemoveButton = new JButton("Remove Account");
	JButton accountEditButton = new JButton("Edit Account");
	JButton accountBackButton = new JButton("Back to menu");

	JButton operationWithdrawButton = new JButton("Withdraw");
	JButton operationDepositButton = new JButton("Deposit");
	JButton operationBackButton = new JButton("Back to menu");

	// TEXT FIELDS
	JTextField clientIdTextField = new JTextField();
	JTextField clientNameTextField = new JTextField();
	JTextField clientMailTextField = new JTextField();
	JTextField clientPhoneTextField = new JTextField();
	JTextField clientAgeTextField = new JTextField();

	JTextField accountIdTextField = new JTextField();
	JTextField accountClientIdTextField = new JTextField();
	JTextField accountTypeTextField = new JTextField();
	JTextField accountAmountTextField = new JTextField();

	JTextField operationClientIdTextField = new JTextField();
	JTextField operationAccountIdTextField = new JTextField();
	JTextField operationAmountTextField = new JTextField();
	
	JFrame frame1 = new JFrame();
	JFrame frame2 = new JFrame();
	JFrame frame3 = new JFrame();
	JFrame frame4 = new JFrame();

	final JPanel mainPanel = new JPanel();
/**
 * The constructor of the graphical interface.
 * @return 
 * @throws ClassNotFoundException
 * @throws InstantiationException
 * @throws IllegalAccessException
 * @throws UnsupportedLookAndFeelException
 * @throws IOException 
 */
	public functionFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException, IOException {

		UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

		frame1.setVisible(true);
		frame1.setSize(600, 600);
		frame1.setTitle("Operation Manager"); // title
		frame1.setResizable(false); // not allows the user to extend the window

		// create panel

		mainPanel.setLayout(null);
		// Main menu
		frame1.add(clientButton);
		frame1.add(accountButton);
		frame1.add(operationButton);
		clientButton.setBounds(150, 100, 300, 80);
		clientButton.setForeground(Color.BLACK);
		clientButton.setFont(clientButton.getFont().deriveFont(20.0f));
		accountButton.setBounds(150, 250, 300, 80);
		accountButton.setForeground(Color.BLACK);
		accountButton.setFont(accountButton.getFont().deriveFont(20.0f));
		operationButton.setBounds(150, 400, 300, 80);
		operationButton.setForeground(Color.BLACK);
		operationButton.setFont(operationButton.getFont().deriveFont(20.0f));
		frame1.add(mainPanel);
		
		final Bank bank = new Bank();

		clientButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				frame2.setVisible(true);
				frame2.setSize(720, 700);
				frame2.pack();
				frame2.dispose();
				final JPanel clientPanel = new JPanel();
				
				
				clientPanel.setLayout(null);
				clientPanel.setVisible(true);
				frame2.add(clientPanel);
				mainPanel.setVisible(false);
				setContentPane(clientPanel);
				setSize(720, 700);
				setTitle("Clients");
				

				// Client
				clientPanel.add(clientIdLabel);
					clientIdLabel.setBounds(40, 400, 100, 40);
					clientIdLabel.setForeground(Color.BLACK);
					clientIdLabel.setFont(clientIdLabel.getFont().deriveFont(20.0f));
				clientPanel.add(clientNameLabel);
					clientNameLabel.setBounds(180, 400, 100, 40);
					clientNameLabel.setForeground(Color.BLACK);
					clientNameLabel.setFont(clientNameLabel.getFont().deriveFont(20.0f));
				clientPanel.add(clientMailLabel);
					clientMailLabel.setBounds(320, 400, 100, 40);
					clientMailLabel.setForeground(Color.BLACK);
					clientMailLabel.setFont(clientMailLabel.getFont().deriveFont(20.0f));
				clientPanel.add(clientPhoneLabel);
					clientPhoneLabel.setBounds(460, 400, 100, 40);
					clientPhoneLabel.setForeground(Color.BLACK);
					clientPhoneLabel.setFont(clientPhoneLabel.getFont().deriveFont(20.0f));
				clientPanel.add(clientAgeLabel);
					clientAgeLabel.setBounds(600, 400, 100, 40);
					clientAgeLabel.setForeground(Color.BLACK);
					clientAgeLabel.setFont(clientAgeLabel.getFont().deriveFont(20.0f));
				clientPanel.add(clientIdTextField);
					clientIdTextField.setBounds(40, 460, 100, 40);
					clientIdTextField.setForeground(Color.BLACK);
					clientIdTextField.setFont(clientIdTextField.getFont().deriveFont(20.0f));
				clientPanel.add(clientNameTextField);
					clientNameTextField.setBounds(180, 460, 100, 40);
					clientNameTextField.setForeground(Color.BLACK);
					clientNameTextField.setFont(clientNameTextField.getFont().deriveFont(20.0f));
				clientPanel.add(clientMailTextField);
					clientMailTextField.setBounds(320, 460, 100, 40);
					clientMailTextField.setForeground(Color.BLACK);
					clientMailTextField.setFont(clientMailTextField.getFont().deriveFont(20.0f));
				clientPanel.add(clientPhoneTextField);
					clientPhoneTextField.setBounds(460, 460, 100, 40);
					clientPhoneTextField.setForeground(Color.BLACK);
					clientPhoneTextField.setFont(clientPhoneTextField.getFont().deriveFont(20.0f));
				clientPanel.add(clientAgeTextField);
					clientAgeTextField.setBounds(600, 460, 100, 40);
					clientAgeTextField.setForeground(Color.BLACK);
					clientAgeTextField.setFont(clientAgeTextField.getFont().deriveFont(20.0f));
				clientPanel.add(clientAddButton);
					clientAddButton.setBounds(40, 560, 150, 40);
					clientAddButton.setForeground(Color.BLACK);
					clientAddButton.setFont(clientAddButton.getFont().deriveFont(17.0f));
				clientPanel.add(clientRemoveButton);
					clientRemoveButton.setBounds(200, 560, 150, 40);
					clientRemoveButton.setForeground(Color.BLACK);
					clientRemoveButton.setFont(clientRemoveButton.getFont().deriveFont(17.0f));
				clientPanel.add(clientEditButton);
					clientEditButton.setBounds(360, 560, 150, 40);
					clientEditButton.setForeground(Color.BLACK);
					clientEditButton.setFont(clientEditButton.getFont().deriveFont(17.0f));
				clientPanel.add(clientBackButton);
					clientBackButton.setBounds(520, 560, 150, 40);
					clientBackButton.setForeground(Color.BLACK);
					clientBackButton.setFont(clientBackButton.getFont().deriveFont(17.0f));
				JTabelValuePopulator pop = new JTabelValuePopulator();
				final JTable createTable = pop.createTable(Client.class, bank.findAll());
				clientPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
				
				createTable.addMouseListener(new java.awt.event.MouseAdapter() {
				    @Override
				    public void mouseClicked(java.awt.event.MouseEvent evt) {
				        int row = createTable.rowAtPoint(evt.getPoint());
				        int col = createTable.columnAtPoint(evt.getPoint());
				        JOptionPane.showMessageDialog(null,"The cell you clicked on is: "+createTable.getValueAt(row,col).toString());
				    }
				});

				clientBackButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						frame2.setVisible(false);
						frame1.setVisible(true);
						clientPanel.setVisible(false);
						mainPanel.setVisible(true);
						setContentPane(mainPanel);
						setSize(600, 600);
						setTitle("Operation Manager");
					}

				});
				clientAddButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						bank.addClient(Integer.parseInt(clientIdTextField.getText()),
								clientNameTextField.getText(), clientMailTextField.getText(),
								clientPhoneTextField.getText(), Integer.parseInt(clientAgeTextField.getText()));
						JTabelValuePopulator pop = new JTabelValuePopulator();
						JTable createTable = pop.createTable(Client.class, bank.findAll());
						clientPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
						clientIdTextField.setText("");
						clientNameTextField.setText("");
						clientMailTextField.setText("");
						clientPhoneTextField.setText("");
						clientAgeTextField.setText("");
					}
				});

				clientEditButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						bank.editClient(Integer.parseInt(clientIdTextField.getText()),
								clientNameTextField.getText(), clientMailTextField.getText(),
								clientPhoneTextField.getText(), Integer.parseInt(clientAgeTextField.getText()));
						JTabelValuePopulator pop = new JTabelValuePopulator();
						JTable createTable = pop.createTable(Client.class, bank.findAll());
						clientPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
						clientIdTextField.setText("");
						clientNameTextField.setText("");
						clientMailTextField.setText("");
						clientPhoneTextField.setText("");
						clientAgeTextField.setText("");
					}
				});

				clientRemoveButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						bank.removeClient(Integer.parseInt(clientIdTextField.getText()));
						JTabelValuePopulator pop = new JTabelValuePopulator();
						JTable createTable = pop.createTable(Client.class, bank.findAll());
						clientPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
						clientIdTextField.setText("");
						clientNameTextField.setText("");
						clientMailTextField.setText("");
						clientPhoneTextField.setText("");
						clientAgeTextField.setText("");
					}
				});
			}
		});

		accountButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame3.setVisible(true);
				frame3.setSize(720, 700);
				frame3.pack();
				frame3.dispose();
				final JPanel productPanel = new JPanel();
				frame3.add(productPanel);
				productPanel.setLayout(null);
				productPanel.setVisible(true);
				mainPanel.setVisible(false);
				setContentPane(productPanel);
				setSize(720, 700);
				setTitle("Accounts");

				productPanel.add(accountIdLabel);
					accountIdLabel.setBounds(40, 400, 120, 40);
					accountIdLabel.setForeground(Color.BLACK);
					accountIdLabel.setFont(accountIdLabel.getFont().deriveFont(20.0f));
				productPanel.add(accountClientIdLabel);
					accountClientIdLabel.setBounds(180, 400, 100, 40);
					accountClientIdLabel.setForeground(Color.BLACK);
					accountClientIdLabel.setFont(accountClientIdLabel.getFont().deriveFont(20.0f));
				productPanel.add(accountTypeLabel);
					accountTypeLabel.setBounds(320, 400, 100, 40);
					accountTypeLabel.setForeground(Color.BLACK);
					accountTypeLabel.setFont(accountTypeLabel.getFont().deriveFont(20.0f));
				productPanel.add(accountAmountLabel);
					accountAmountLabel.setBounds(460, 400, 100, 40);
					accountAmountLabel.setForeground(Color.BLACK);
					accountAmountLabel.setFont(accountAmountLabel.getFont().deriveFont(20.0f));
				productPanel.add(accountIdTextField);
					accountIdTextField.setBounds(40, 460, 100, 40);
					accountIdTextField.setForeground(Color.BLACK);
					accountIdTextField.setFont(accountIdTextField.getFont().deriveFont(20.0f));
				productPanel.add(accountClientIdTextField);
					accountClientIdTextField.setBounds(180, 460, 100, 40);
					accountClientIdTextField.setForeground(Color.BLACK);
					accountClientIdTextField.setFont(accountClientIdTextField.getFont().deriveFont(20.0f));
				productPanel.add(accountTypeTextField);
					accountTypeTextField.setBounds(320, 460, 100, 40);
					accountTypeTextField.setForeground(Color.BLACK);
					accountTypeTextField.setFont(accountTypeTextField.getFont().deriveFont(20.0f));
				productPanel.add(accountAmountTextField);
					accountAmountTextField.setBounds(460, 460, 100, 40);
					accountAmountTextField.setForeground(Color.BLACK);
					accountAmountTextField.setFont(accountAmountTextField.getFont().deriveFont(20.0f));
				productPanel.add(accountAddButton);
					accountAddButton.setBounds(40, 560, 140, 40);
					accountAddButton.setForeground(Color.BLACK);
					accountAddButton.setFont(accountAddButton.getFont().deriveFont(16.0f));
					productPanel.add(accountRemoveButton);
					accountRemoveButton.setBounds(190, 560, 160, 40);
					accountRemoveButton.setForeground(Color.BLACK);
					accountRemoveButton.setFont(accountRemoveButton.getFont().deriveFont(15.0f));
				productPanel.add(accountEditButton);
					accountEditButton.setBounds(360, 560, 150, 40);
					accountEditButton.setForeground(Color.BLACK);
					accountEditButton.setFont(accountEditButton.getFont().deriveFont(17.0f));
				productPanel.add(accountBackButton);
					accountBackButton.setBounds(520, 560, 150, 40);
					accountBackButton.setForeground(Color.BLACK);
					accountBackButton.setFont(accountBackButton.getFont().deriveFont(17.0f));
				JTabelValuePopulator pop = new JTabelValuePopulator();
				ArrayList<Account> accList = bank.findAllAccounts();
				final JTable createTable = pop.createTable(Account.class, accList);
				productPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
				
				createTable.addMouseListener(new java.awt.event.MouseAdapter() {
				    @Override
				    public void mouseClicked(java.awt.event.MouseEvent evt) {
				        int row = createTable.rowAtPoint(evt.getPoint());
				        int col = createTable.columnAtPoint(evt.getPoint());
				        JOptionPane.showMessageDialog(null,"The cell you clicked on is: "+createTable.getValueAt(row,col).toString());
				    }
				});

				accountBackButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						frame1.setVisible(true);
						frame3.setVisible(false);
						productPanel.setVisible(false);
						mainPanel.setVisible(true);
						setContentPane(mainPanel);
						setSize(600, 600);
						setTitle("Operation Manager");
					}
				});
				
				accountAddButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Client c = bank.findById(Integer.parseInt(accountClientIdTextField.getText()));
						bank.addAccount(c, Integer.parseInt(accountIdTextField.getText()),
								Integer.parseInt(accountClientIdTextField.getText()), accountTypeTextField.getText(),
								Integer.parseInt(accountAmountTextField.getText()));
						JTabelValuePopulator pop = new JTabelValuePopulator();
						ArrayList<Account> accList = bank.findAllAccounts();
						JTable createTable = pop.createTable(Account.class, accList);
						productPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
						accountIdTextField.setText("");
						accountClientIdTextField.setText("");
						accountTypeTextField.setText("");
						accountAmountTextField.setText("");
					}
				});

				accountEditButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Client c = bank.findById(Integer.parseInt(accountClientIdTextField.getText()));
						bank.editAccount(c, Integer.parseInt(accountIdTextField.getText()),
								Integer.parseInt(accountClientIdTextField.getText()), accountTypeTextField.getText(),
								Integer.parseInt(accountAmountTextField.getText()));
						JTabelValuePopulator pop = new JTabelValuePopulator();
						ArrayList<Account> accList = bank.findAllAccounts();
						JTable createTable = pop.createTable(Account.class, accList);
						productPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
						accountIdTextField.setText("");
						accountClientIdTextField.setText("");
						accountTypeTextField.setText("");
						accountAmountTextField.setText("");
					}
				});

				accountRemoveButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Client c = bank.findById(Integer.parseInt(accountClientIdTextField.getText()));
						bank.removeAccount(c, Integer.parseInt(accountIdTextField.getText()));
						JTabelValuePopulator pop = new JTabelValuePopulator();
						ArrayList<Account> accList = bank.findAllAccounts();
						JTable createTable = pop.createTable(Account.class, accList);
						productPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
						accountIdTextField.setText("");
						accountClientIdTextField.setText("");
						accountTypeTextField.setText("");
						accountAmountTextField.setText("");
					}
				});
			}
		});

		operationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame4.setVisible(true);
				frame4.setSize(720, 700);
				frame4.pack();
				frame4.dispose();
				final JPanel operationPanel = new JPanel();
				frame4.add(operationPanel);
				operationPanel.setLayout(null);
				operationPanel.setVisible(true);
				mainPanel.setVisible(false);
				setContentPane(operationPanel);
				setSize(720, 700);
				setTitle("Operation");

				operationPanel.add(operationAccountIdLabel);
					operationAccountIdLabel.setBounds(40, 400, 100, 40);
					operationAccountIdLabel.setForeground(Color.BLACK);
					operationAccountIdLabel.setFont(operationAccountIdLabel.getFont().deriveFont(20.0f));
				operationPanel.add(operationAccountClientIdLabel);
					operationAccountClientIdLabel.setBounds(180, 400, 100, 40);
					operationAccountClientIdLabel.setForeground(Color.BLACK);
					operationAccountClientIdLabel.setFont(operationAccountClientIdLabel.getFont().deriveFont(20.0f));
				operationPanel.add(operationAmountLabel);
					operationAmountLabel.setBounds(320, 400, 100, 40);
					operationAmountLabel.setForeground(Color.BLACK);
					operationAmountLabel.setFont(operationAmountLabel.getFont().deriveFont(20.0f));
				operationPanel.add(operationAccountIdTextField);
					operationAccountIdTextField.setBounds(40, 460, 100, 40);
					operationAccountIdTextField.setForeground(Color.BLACK);
					operationAccountIdTextField.setFont(operationAccountIdTextField.getFont().deriveFont(20.0f));
				operationPanel.add(operationClientIdTextField);
					operationClientIdTextField.setBounds(180, 460, 100, 40);
					operationClientIdTextField.setForeground(Color.BLACK);
					operationClientIdTextField.setFont(operationClientIdTextField.getFont().deriveFont(20.0f));
				operationPanel.add(operationAmountTextField);
					operationAmountTextField.setBounds(320, 460, 100, 40);
					operationAmountTextField.setForeground(Color.BLACK);
					operationAmountTextField.setFont(operationAmountTextField.getFont().deriveFont(20.0f));
				operationPanel.add(operationWithdrawButton);
					operationWithdrawButton.setBounds(40, 560, 130, 40);
					operationWithdrawButton.setForeground(Color.BLACK);
					operationWithdrawButton.setFont(operationWithdrawButton.getFont().deriveFont(17.0f));
				operationPanel.add(operationDepositButton);
					operationDepositButton.setBounds(180, 560, 170, 40);
					operationDepositButton.setForeground(Color.BLACK);
					operationDepositButton.setFont(operationDepositButton.getFont().deriveFont(17.0f));
				operationPanel.add(operationBackButton);
					operationBackButton.setBounds(360, 560, 150, 40);
					operationBackButton.setForeground(Color.BLACK);
					operationBackButton.setFont(operationBackButton.getFont().deriveFont(16.0f));

					JTabelValuePopulator pop = new JTabelValuePopulator();
					ArrayList<Account> accList = bank.findAllAccounts();
					final JTable createTable = pop.createTable(Account.class, accList);
					operationPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
					createTable.addMouseListener(new java.awt.event.MouseAdapter() {
					    @Override
					    public void mouseClicked(java.awt.event.MouseEvent evt) {
					        int row = createTable.rowAtPoint(evt.getPoint());
					        int col = createTable.columnAtPoint(evt.getPoint());
					        JOptionPane.showMessageDialog(null,"The cell you clicked on is: "+createTable.getValueAt(row,col).toString());
					    }
					});
					
				operationBackButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						frame1.setVisible(true);
						frame4.setVisible(false);
						operationPanel.setVisible(false);
						mainPanel.setVisible(true);
						setContentPane(mainPanel);
						setSize(600, 600);
						setTitle("Operation Manager");
					}

				});

				operationWithdrawButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int id = Integer.parseInt(operationAccountIdTextField.getText());
						Client client = bank.findById(Integer.parseInt(operationClientIdTextField.getText()));
						Account account = bank.findAccountById(client, id);
						System.out.println(account.getAccountType());
						if (account.getAccountType().equals("saving") && counterWithdraw == 0) {
							SavingAccount savingAccount = new SavingAccount(account.getAccountId(),account.getAccountClientId(),account.getAccountType(),account.getAccountAmount());
							bank.editAccount(client, account.getAccountId(),account.getAccountClientId(),account.getAccountType(), savingAccount.withdraw());
							verifySavingWithdraw();
						}else if(account.getAccountType().equals("saving") && counterWithdraw != 0) {
							System.out.println("Already withdraw!");
						}else if(account.getAccountType().equals("spending")) {
							SpendingAccount spendingAccount = new SpendingAccount(account.getAccountId(),account.getAccountClientId(),account.getAccountType(),account.getAccountAmount());
							bank.editAccount(client, account.getAccountId(),account.getAccountClientId(),account.getAccountType(), spendingAccount.withdraw(Integer.parseInt(operationAmountTextField.getText())));
						}else {
							System.out.println("Wrong Type!");
						}
						JTabelValuePopulator pop = new JTabelValuePopulator();
						ArrayList<Account> accList = bank.findAllAccounts();
						JTable createTable = pop.createTable(Account.class, accList);
						operationPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
						operationAccountIdTextField.setText("");
						operationClientIdTextField.setText("");
						operationAmountTextField.setText("");
					}
				});

				
				operationDepositButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int id = Integer.parseInt(operationAccountIdTextField.getText());
						Client client = bank.findById(Integer.parseInt(operationClientIdTextField.getText()));
						Account account = bank.findAccountById(client, id);
						System.out.println(account.getAccountType());
						if (account.getAccountType().equals("saving") && counterDeposit == 0) {
							SavingAccount savingAccount = new SavingAccount(account.getAccountId(),account.getAccountClientId(),account.getAccountType(),account.getAccountAmount());
							bank.editAccount(client, account.getAccountId(),account.getAccountClientId(),account.getAccountType(), savingAccount.deposit(savingAccount.getAccountAmount()+Integer.parseInt(operationAmountTextField.getText())));
							verifySavingDeposit();
						}else if(account.getAccountType().equals("saving") && counterDeposit != 0) {
							System.out.println("Already made a deposit!");
						}else if(account.getAccountType().equals("spending")) {
							SpendingAccount spendingAccount = new SpendingAccount(account.getAccountId(),account.getAccountClientId(),account.getAccountType(),account.getAccountAmount());
							bank.editAccount(client, account.getAccountId(),account.getAccountClientId(),account.getAccountType(), spendingAccount.deposit(Integer.parseInt(operationAmountTextField.getText())));
						}else {
							System.out.println("Wrong Type!");
						}
						JTabelValuePopulator pop = new JTabelValuePopulator();
						ArrayList<Account> accList = bank.findAllAccounts();
						JTable createTable = pop.createTable(Account.class, accList);
						operationPanel.add(new JScrollPane(createTable)).setBounds(100, 100, 550, 200);
						operationAccountIdTextField.setText("");
						operationAmountTextField.setText("");
					}
				});

			}
		});

		this.setContentPane(mainPanel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public int counterDeposit = 0;
	public int counterWithdraw = 0;
	
	public int verifySavingDeposit() {
		counterDeposit++;
		return counterDeposit;
	}
	public int verifySavingWithdraw() {
		counterWithdraw++;
		return counterWithdraw;
	}


	// public void customerInterface()
	// {
	// clientIdLabel.setVisible(true);
	// clientNameLabel.setVisible(true);
	// clientMailLabel.setVisible(true);
	// clientPhoneLabel.setVisible(true);
	// clientAgeLabel.setVisible(true);
	// clientIdTextField.setVisible(true);
	// clientNameTextField.setVisible(true);
	// clientMailTextField.setVisible(true);
	// clientPhoneTextField.setVisible(true);
	// clientAgeTextField.setVisible(true);
	// clientAddButton.setVisible(true);
	// clientRemoveButton.setVisible(true);
	// clientUpdateButton.setVisible(true);
	// clientBackButton.setVisible(true);
	// }
	// public void hideCustomerInterface()
	// {
	// clientIdLabel.setVisible(false);
	// clientNameLabel.setVisible(false);
	// clientMailLabel.setVisible(false);
	// clientPhoneLabel.setVisible(false);
	// clientAgeLabel.setVisible(false);
	// clientIdTextField.setVisible(false);
	// clientNameTextField.setVisible(false);
	// clientMailTextField.setVisible(false);
	// clientPhoneTextField.setVisible(false);
	// clientAgeTextField.setVisible(false);
	// clientAddButton.setVisible(false);
	// clientRemoveButton.setVisible(false);
	// clientUpdateButton.setVisible(false);
	// clientBackButton.setVisible(false);
	// }
	//
	// public void productInterface()
	// {
	// productIdLabel.setVisible(true);
	// productNameLabel.setVisible(true);
	// productPriceLabel.setVisible(true);
	// productStockLabel.setVisible(true);
	// productIdTextField.setVisible(true);
	// productNameTextField.setVisible(true);
	// productPriceTextField.setVisible(true);
	// productStockTextField.setVisible(true);
	// productAddButton.setVisible(true);
	// productRemoveButton.setVisible(true);
	// productUpdateButton.setVisible(true);
	// productBackButton.setVisible(true);
	// }
	// public void hideProductInterface()
	// {
	// productIdLabel.setVisible(false);
	// productNameLabel.setVisible(false);
	// productPriceLabel.setVisible(false);
	// productStockLabel.setVisible(false);
	// productIdTextField.setVisible(false);
	// productNameTextField.setVisible(false);
	// productPriceTextField.setVisible(false);
	// productStockTextField.setVisible(false);
	// productAddButton.setVisible(false);
	// productRemoveButton.setVisible(false);
	// productUpdateButton.setVisible(false);
	// productBackButton.setVisible(false);
	// }
	//
	// public void orderInterface()
	// {
	// orderIdLabel.setVisible(true);
	// orderClientIdLabel.setVisible(true);
	// orderProductIdLabel.setVisible(true);
	// orderQuantityLabel.setVisible(true);
	// orderIdTextField.setVisible(true);
	// orderClientIdTextField.setVisible(true);
	// orderProductIdTextField.setVisible(true);
	// orderQuantityTextField.setVisible(true);
	// orderAddButton.setVisible(true);
	// orderRemoveButton.setVisible(true);
	// orderBackButton.setVisible(true);
	// }
	// public void hideOrderInterface()
	// {
	// orderIdLabel.setVisible(false);
	// orderClientIdLabel.setVisible(false);
	// orderProductIdLabel.setVisible(false);
	// orderQuantityLabel.setVisible(false);
	// orderIdTextField.setVisible(false);
	// orderClientIdTextField.setVisible(false);
	// orderProductIdTextField.setVisible(false);
	// orderQuantityTextField.setVisible(false);
	// orderAddButton.setVisible(false);
	// orderRemoveButton.setVisible(false);
	// orderBackButton.setVisible(false);
	// }
	//
	// public void menuInterface()
	// {
	// clientButton.setVisible(true);
	// productButton.setVisible(true);
	// orderButton.setVisible(true);
	// }
	// public void hideMenuInterface()
	// {
	// clientButton.setVisible(false);
	// productButton.setVisible(false);
	// orderButton.setVisible(false);
	// }
/**
 * The main class starting the simulation
 * @param args
 * @throws ClassNotFoundException
 * @throws InstantiationException
 * @throws IllegalAccessException
 * @throws UnsupportedLookAndFeelException
 * @throws IOException 
 */
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException, IOException {
//		File file = new File("Receipt.txt");
//		FileOutputStream fos = new FileOutputStream(file);
//		PrintStream ps = new PrintStream(fos);
//		System.setOut(ps);
		functionFrame aux = new functionFrame();
		aux.setVisible(true);
	}
}
