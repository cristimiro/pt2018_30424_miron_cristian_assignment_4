package mainPackage;

public class SpendingAccount extends Account {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7577827004032994838L;

	public SpendingAccount(int accountId, int accountClientId, String accountType, int accountAmount) {
		super(accountId, accountClientId, accountType, accountAmount);
	}

	public int withdraw(int sumToWithdraw) {
		setChanged();
		notifyObservers(accountAmount);
		int sum = getAccountAmount() - sumToWithdraw;
		return sum;
	}

	public int deposit(int sumToDeposit) {
		setChanged();
		notifyObservers(accountAmount);
		int sum = getAccountAmount() + sumToDeposit;
		return sum;
	}

}
