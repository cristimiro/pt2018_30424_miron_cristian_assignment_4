package mainPackage;

public interface BankProc {

	/**
	 * 
	 * @pre clientId >= 0
	 * @pre clientName!=null
	 * @pre clientMail!=null
	 * @pre clientPhoneNumber != null
	 * @pre clientAge >= 0
	 * @post bank is ok
	 */
	public void addClient(int clientId, String clientName, String clientMail, String clientPhoneNumber, int clientAge);
	/**
	 * 
	 * @pre clientId >= 0
	 * @post bank is ok
	 */
	public void removeClient(int clientId);
	/**
	 * @pre clientId >= 0
	 * @pre clientName!=null
	 * @pre clientMail!=null
	 * @pre clientPhoneNumber != null
	 * @pre clientAge >= 0
	 * @post bank is ok
	 */
	public void editClient(int clientId, String clientName, String clientMail, String clientPhoneNumber, int clientAge);
	/**
	 * @pre client != null
	 * @pre accountId>=0
	 * @pre accountClientId>=0
	 * @pre accountType != null
	 * @pre accountAmount >= 0
	 * @post bank is ok
	 */
	public void addAccount(Client client, int accountId, int accountClientId, String accountType, int accountAmount);
	/**
	 * 
	 * @pre client!= null
	 * @pre accountId>=0
	 * @post bank is ok
	 */
	public void removeAccount(Client client, int accountId);
	/**
	 * @pre client != null
	 * @pre accountId>=0
	 * @pre accountClientId>=0
	 * @pre accountType != null
	 * @pre accountAmount >= 0
	 * @post bank is ok
	 */
	public void editAccount(Client client, int accountId, int accountClientId, String accountType, int accountAmount);

}
