package mainPackage;

public class SavingAccount extends Account {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5618262594843142920L;

	public SavingAccount(int accountId, int accountClientId, String accountType, int accountAmount) {
		super(accountId, accountClientId, accountType, accountAmount);
	}

	public int withdraw() {
		hasChanged();
		notifyObservers(accountAmount);
		return 0;
	}

	public int deposit(int sumToDeposit) {
		hasChanged();
		notifyObservers(accountAmount);
		sumToDeposit += sumToDeposit * 0.1;
		return sumToDeposit;
	}

}
