package mainPackage;

import java.io.Serializable;
import java.util.Observable;

public class Account extends Observable implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private int accountId;
	private int accountClientId;
	private String accountType;
	protected int accountAmount;

	public Account(int accountId, int accountClientId, String accountType, int accountAmount) {
		this.accountId = accountId;
		this.accountClientId = accountClientId;
		this.accountType = accountType;
		this.accountAmount = accountAmount;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public int getAccountClientId() {
		return accountClientId;
	}

	public void setAccountClientId(int accountClientId) {
		this.accountClientId = accountClientId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public int getAccountAmount() {
		return accountAmount;
	}

	public void setAccountAmount(int accountAmount) {
		this.accountAmount = accountAmount;
	}
	
	public String toString() {
		String s = "";
		s = " ID - "+getAccountId()+", ClientId- "+getAccountClientId()+", AccountType - "+getAccountType()+", Amount - "+getAccountAmount()+".";
		return s;
	}

}
