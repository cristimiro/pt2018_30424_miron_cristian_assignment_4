package mainPackage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Bank implements BankProc {

	HashMap<Client, HashSet<Account>> bankList = new HashMap<Client, HashSet<Account>>();

	public Bank() {
		try {
			deserialize();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean wellFormed() {
		if (this.bankList != null) {
			return true;
		}else {
			return false;
		}
	}

	private void serialize() {
        // Method for serialization of object
        try {
    		String fileName = "output.txt";
    		FileOutputStream file = new FileOutputStream(fileName);
            ObjectOutputStream out = new ObjectOutputStream(file);
			out.writeObject(this.bankList);
			
	        out.close();
	        file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void deserialize() throws IOException, ClassNotFoundException{
		String fileName = "output.txt";
		FileInputStream file;
		try {
			file = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(file);
			this.bankList = (HashMap<Client, HashSet<Account>>) in.readObject();
			in.close();
			file.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void addClient(int clientId, String clientName, String clientMail, String clientPhoneNumber, int clientAge) {
		assert (wellFormed()) : "Bank is not well formed";
		assert (clientId<0)  : "Client id is not valid";
		HashSet<Account> accountList = new HashSet<Account>();
		Client client = new Client(clientId, clientName, clientMail, clientPhoneNumber, clientAge);
		assert (client == null): "Client does not exisit";
		bankList.put(client, accountList);
		serialize();
	}

	public void removeClient(int clientId) {
		assert (wellFormed()) : "Bank is not well formed";
		assert (clientId<0)  : "Client id is not valid";
		Client client = new Client(clientId, null, null, null, 0);
		assert (client == null): "Client does not exisit";
		bankList.remove(client);
		serialize();
	}

	public void editClient(int clientId, String clientName, String clientMail, String clientPhoneNumber,
			int clientAge) {
		assert (wellFormed()) : "Bank is not well formed";
		assert (clientId<0)  : "Client id is not valid";
		HashSet<Account> accountList = new HashSet<Account>();
		Client client = new Client(clientId, clientName, clientMail, clientPhoneNumber, clientAge);
		assert (client == null): "Client does not exisit";
		Client client1 = new Client(clientId, null, null, null, 0);
		assert (client1 == null): "Client does not exisit";
		bankList.remove(client1);
		bankList.put(client, accountList);
		serialize();
	}

	public void addAccount(Client client, int accountId, int accountClientId, String accountType, int accountAmount) {
		assert (wellFormed()) : "Bank is not well formed";
		assert (client == null): "Client does not exisit";
		assert (accountId<0)  : "Client id is not valid";
		Account account = new Account(accountId, accountClientId, accountType, accountAmount);
		bankList.get(client).add(account);
		account.addObserver(client);
		serialize();
	}

	public void removeAccount(Client client, int accountId) {
		assert (wellFormed()) : "Bank is not well formed";
		assert (client == null): "Client does not exisit";
		assert (accountId<0)  : "Client id is not valid";
		Account a = findAccountById(client, accountId);
		bankList.get(client).remove(a);
		serialize();
	}

	public void editAccount(Client client, int accountId, int accountClientId, String accountType, int accountAmount) {
		assert (wellFormed()) : "Bank is not well formed";
		assert (client == null): "Client does not exisit";
		assert (accountId<0)  : "Client id is not valid";
		Account a = findAccountById(client, accountId);
		assert (a == null): "Account does not exisit";
		bankList.get(client).remove(a);
		Account account = new Account(accountId, accountClientId, accountType, accountAmount);
		assert (account == null): "Account does not exisit";
		bankList.get(client).add(account);
		serialize();
	}

	public Client findById(int clientId) {
		Client client = null;
		for (Client c : this.bankList.keySet()) {
			if (c.getClientId() == clientId) {
				client = c;
			}
		}
		return client;
	}
	

	public Account findAccountById(Client client, int accountId) {
		Account account = null;
		for (Account a : this.bankList.get(client)) {
			if (a.getAccountId() == accountId) {
				account = a;
			}
		}
		return account;
	}

	public ArrayList<Client> findAll() {
		ArrayList<Client> clientList = new ArrayList<Client>();
		for (Client c : this.bankList.keySet()) {
			clientList.add(c);
		}
		return clientList;
	}

	public ArrayList<Account> findAllAccounts() {
		ArrayList<Account> accList = new ArrayList<Account>();
		for (HashSet<Account> a : this.bankList.values()) {
			accList.addAll(a);
		}
		return accList;
	}

	public HashMap<Client, HashSet<Account>> getBankList() {
		return bankList;
	}

	public void setBankList(HashMap<Client, HashSet<Account>> bankList) {
		this.bankList = bankList;
	}

}
