package PT2018.demo.Assignment_4;

import org.junit.Assert;
import org.junit.Test;

import mainPackage.Account;
import mainPackage.Bank;
import mainPackage.Client;


public class AppTest  {
	
//FIRST TEST - ADD CLIENT
	@Test
	public void addClientTest_ReturnOK () {
		String expectedResult = " ID - 1, Name - Ion, Mail - ion@gmail.com, Phone - 0777777777, age - 20.";
		Client client = new Client(1,"Ion","ion@gmail.com","0777777777",20);
		Bank bank = new Bank();
		bank.addClient(client.getClientId(),client.getClientName(),client.getClientMail(),client.getClientPhoneNumber(),client.getClientAge());
		String s = bank.findById(1).toString();
		Assert.assertEquals(expectedResult, s);
	}
	
//SECOND TEST -ADD ACCOUNT
	@Test
	public void addAccountTest_ReturnOk () {
		String expectedResult = " ID - 1, ClientId- 1, AccountType - saving, Amount - 100.";
		Account account = new Account(1,1,"saving",100);
		Bank bank = new Bank();
		Client client = bank.findById(1);
		bank.addAccount(client,account.getAccountId(),account.getAccountClientId(),account.getAccountType(),account.getAccountAmount());
		String s = bank.findAccountById(client, account.getAccountId()).toString();
		Assert.assertEquals(expectedResult, s);
	}
//	
//THIRD TEST - EDIT CLIENT
	@Test
	public void editClientTest_ReturnOk () {
		String expectedResult = " ID - 2, Name - IonAfterEdit, Mail - ion@gmail.com, Phone - 0777777777, age - 20.";
		Bank bank = new Bank();
		Client client = new Client(2,"IonAfterEdit","ion@gmail.com","0777777777",20);
		bank.editClient(client.getClientId(),client.getClientName(),client.getClientMail(),client.getClientPhoneNumber(),client.getClientAge());
		String s = bank.findById(2).toString();
		Assert.assertEquals(expectedResult, s);
	}	
//	
//FOURTH TEST - EDIT ACCOUNT	
	@Test
	public void editAccountTest_ReturnOk () {
		String expectedResult = " ID - 2, ClientId- 1, AccountType - spending, Amount - 100.";
		Account account = new Account(2,1,"spending",100);
		Bank bank = new Bank();
		Client client = bank.findById(1);
		bank.addAccount(client,account.getAccountId(),account.getAccountClientId(),account.getAccountType(),account.getAccountAmount());
		String s = bank.findAccountById(client, account.getAccountId()).toString();
		Assert.assertEquals(expectedResult, s);
	}
//		
//	//SECOND TEST - Multiplication
//	@Test
//	public void polynomTest_MultiplicationFunction_ReturnOk () {
//		String expectedResult = "+18x^7-18x^4";
//		Polynom firstPolynom = new Polynom();
//		Polynom secondPolynom = new Polynom();
//		Monom firstMonomOfFirstPolynom = new Monom(3, 2);
//		Monom firstMonomOfSecondPolynom = new Monom(6, 5);
//		Monom secondMonomOfSecondPolynom = new Monom(-6, 2);
//		firstPolynom.addMonomToPolynom(firstMonomOfFirstPolynom);
//		secondPolynom.addMonomToPolynom(secondMonomOfSecondPolynom);
//		secondPolynom.addMonomToPolynom(firstMonomOfSecondPolynom);
//		Polynom result = firstPolynom.multiplyPolynom(secondPolynom);
//		Assert.assertEquals(expectedResult, result.displayPolynom());
//	}
//	
////FIRST TEST - ADD
//	@Test
//	public void polynomTest_DivisionFunction_ReturnOK () {
//		String expectedResult = "+3x+1";
//		Polynom firstPolynom = new Polynom();
//		Polynom secondPolynom = new Polynom();
//		Monom firstMonomOfFirstPolynom = new Monom(3, 2);
//		Monom secondMonomOfFirstPolynom = new Monom(10, 1);
//		Monom thirdMonomOfFirstPolynom = new Monom(5, 0);
//		Monom firstMonomOfSecondPolynom = new Monom(1, 1);
//		Monom secondMonomOfSecondPolynom = new Monom(3, 0);
//		firstPolynom.addMonomToPolynom(firstMonomOfFirstPolynom);
//		firstPolynom.addMonomToPolynom(secondMonomOfFirstPolynom);
//		firstPolynom.addMonomToPolynom(thirdMonomOfFirstPolynom);
//		secondPolynom.addMonomToPolynom(secondMonomOfSecondPolynom);
//		secondPolynom.addMonomToPolynom(firstMonomOfSecondPolynom);
//		Polynom result = firstPolynom.dividePolynom(secondPolynom);
//		Assert.assertEquals(expectedResult, result.displayPolynom());
//		firstPolynom.clearMonomMap();
//	}
}